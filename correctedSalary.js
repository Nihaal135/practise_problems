//4.Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something).

let values = require('./Inventory');

values.forEach((element)=>{
    element['salary'] = parseFloat(element['salary'].replace('$',' '));
    element['correctedSalary'] = '$' + JSON.stringify(element['salary']*10000);
});

console.log(values);