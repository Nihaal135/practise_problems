//6. Find the average salary of based on country using only HOF method
let items = require('./Inventory');

let averageSalaryByCountry = items.reduce((salaryByCountry,currentDetails) =>{
    let salaryInNumber = parseFloat(currentDetails['salary'].slice(1))

    if(salaryByCountry.hasOwnProperty(currentDetails['location'])){
        salaryByCountry[currentDetails['location']] = (salaryByCountry[currentDetails['location']] + salaryInNumber)/2;
    }
    else{
        salaryByCountry[currentDetails['location']] = salaryInNumber;
    }
    
    return salaryByCountry;
},{})

console.log(averageSalaryByCountry);