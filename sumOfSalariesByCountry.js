// 5. Find the sum of all salaries based on country using only HOF method
let inventory = require('./Inventory');

let sumBycountry = inventory.reduce((salaryBycountry,currentDetails) =>{
    let salaryInNumber = parseFloat(currentDetails['salary'].slice(1))

    if(salaryBycountry.hasOwnProperty(currentDetails['location'])){
        salaryBycountry[currentDetails['location']] += salaryInNumber
    }
    else{
        salaryBycountry[currentDetails['location']] = salaryInNumber;
    }
    
    return salaryBycountry
    
},{})

console.log(sumBycountry)

module.exports = sumBycountry;

